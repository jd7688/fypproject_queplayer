//==============================================================================
//==============================================================================
//  QueController.cpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================

#include "QueController.hpp"

//mainstuff=====================================================================
QueController::QueController()//(Audio& audio)	: audio(&audio)
{
    queIndex = 0;
    playbackIndex = 0;
    audioQuePlayerUIArrayIndex = queIndex;
}

QueController::~QueController()
{
    stopAllCues();
}

//queIndex Controls==================================
int QueController::getQueIndex(){return queIndex;}
void QueController::setQueIndex(int newIndex){queIndex = newIndex;}
void QueController::incQueIndex(){queIndex++;}

//queIndex Controls==================================
int QueController::getPlaybackIndex(){return playbackIndex;}
void QueController::setPlaybackIndex(int newIndex){playbackIndex = newIndex;}
void QueController::incPlaybackIndex(){playbackIndex++;}

//playcues======================================================================
void QueController::playNextCue()
{
	DBG("QueController::PlayNextCue");
}

void QueController::stopAllCues()
{
    DBG("QueController::StopAllCues");
}

void QueController::fadeAllCues()
{
    DBG("QueController::FadeAllCues");
    stopAllCues();                                   //Call after fade complete.
}

//Audio==============================================
AudioDeviceManager& QueController::getAudioDeviceManager()
{
    return audio.getAudioDeviceManager();
}

//AudioQueFunctions=============================================================
void QueController::newAudioQue(void)
{
    DBG("QueController::NewAudioQue");
    incQueIndex();
    
	queMemory.newFile(File::nonexistent);
	queMemory.newQuePlayerUI();
    AudioQuePlayerUI& newAudioQuePlayerUI = queMemory.getQuePlayerUI(queIndex);
    
    newAudioQuePlayerUI.setBounds(80, (queIndex * 20) + 5, 420, 20);
}

void QueController::removeAudioQue(void)
{
    DBG("QueController::RemoveAudioQue");
}

AudioQuePlayerUI* QueController::getQue (int index)
{
    DBG("QueController::GetAudioQue");
    return NULL;
}

int QueController::size(void)
{
    return NULL;
}

//==============================================================================
//==============================================================================