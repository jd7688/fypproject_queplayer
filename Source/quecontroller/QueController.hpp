//==============================================================================
//==============================================================================
//  QueController.hpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================

#ifndef QueController_hpp
#define QueController_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../Source/audio/AudioQuePlayer.h"
#include "../../Source/audio/Audio.h"
#include "../Source/ui/AudioQuePlayerUI.h"
#include "QueMemory.h"

class QueController :   public Component
{
public:
    //mainstuff=========================================
    QueController();//(Audio& audio);
    ~QueController();
    
    //queIndex Controls==================================
    int getQueIndex();
    void setQueIndex(int newIndex);
    void incQueIndex();
    
    //queIndex Controls==================================
    int getPlaybackIndex();
    void setPlaybackIndex(int newIndex);
    void incPlaybackIndex();
    
    //playcues===========================================
    void playNextCue();
    void stopAllCues();
    void fadeAllCues();
    
    //Audio==============================================
    AudioDeviceManager& getAudioDeviceManager();
    
    //AudioQueFunctions==================================
    void newAudioQue(void);
    void removeAudioQue(void);
    AudioQuePlayerUI* getQue (int index);
    int size(void);
    
private:
    int queIndex;
    int playbackIndex;
    int audioQuePlayerUIArrayIndex;

//	ScopedPointer<Audio> audio;
    QueMemory queMemory;
    Audio audio;
};

#endif /* QueController_hpp */
//==============================================================================
//==============================================================================