/*==============================================================================
================================================================================
AudioQuePlayerUI.h
==============================================================================*/

#ifndef AUDIOQUEPLAYERUI_H_INCLUDED
#define AUDIOQUEPLAYERUI_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/AudioQuePlayer.h"

class AudioQuePlayerUI : public Component,
						 public Button::Listener,
						 public FilenameComponentListener
{
public:
	//=================================================================
	//MainStuff

    AudioQuePlayerUI();
	
	~AudioQuePlayerUI();

	void resized();

	void buttonClicked(Button* button);
    
    File& returnSavedAudioFile() {return savedAudioFile;}

    void filenameComponentChanged(FilenameComponent* fileComponentThatHasChanged);

private:
	TextButton previewButton;
	FilenameComponent* fileChooser;    
    File savedAudioFile;
};

#endif
//==============================================================================
//==============================================================================