/*==============================================================================
================================================================================
    This file was auto-generated!
==============================================================================*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "../ui/AudioQuePlayerUI.h"
#include "../quecontroller/QueController.hpp"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
						public ButtonListener
{
public:
    //==========================================================================
    /** Constructor */
    MainComponent (QueController& queController);

    /** Destructor */
    ~MainComponent();

    void resized() override;

	//ButtonCode================================================================

    void intButtons();
    void disableGo();
	void buttonClicked(Button* button) override;
	void fadeButtonClicked();
	void goButtonClicked();
	void stopButtonClicked();

    //MenuBarEnums/Callbacks====================================================
    enum Menus
    {
        FileMenu=0,
        CueMenu=1,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
		Save = 2,
		Load = 3,
        
        NumFileItems
    };
    
    enum CueMenuItems
    {
        NewAudioQue = 1,
		NewControlQue = 2,
        
        NumCueItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

private:
	TextButton fadeButton;
	TextButton goButton;
	TextButton stopButton;
    ScopedPointer<QueController> queController;

    //==========================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
//==============================================================================
//==============================================================================



