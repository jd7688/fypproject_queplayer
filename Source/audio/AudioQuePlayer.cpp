/*=================================================================
AudioQuePlayer.cpp
=================================================================*/

#include "AudioQuePlayer.h"

//=================================================================
//MainStuff
AudioQuePlayer::AudioQuePlayer()	:	thread ("FilePlayThread")
{
	//thread.startThread();
	currentAudioFileSource = NULL;
}

AudioQuePlayer::~AudioQuePlayer()
{
	audioTransportSource.setSource(0);
	deleteAndZero(currentAudioFileSource);

	//thread.stopThread(100);
}

void AudioQuePlayer::setPlaying(const bool newState)
{
	if (newState == true)
	{
        DBG("setPlaying");
		audioTransportSource.setPosition(0.0);
		audioTransportSource.start();
	}
	else// if (newState == false)
	{
        DBG("Stop");
		audioTransportSource.stop();
	}
}

bool AudioQuePlayer::isPlaying() const
{
	return audioTransportSource.isPlaying();
}

void AudioQuePlayer::setPlayerState(bool newState)
{
    playerState = newState;
}

bool AudioQuePlayer::isBusy()  //true == busy, false == avalible.
{
    return playerState;
}

bool AudioQuePlayer::loadFile(const File& newFile)
{
	setPlaying(false);
	audioTransportSource.setSource(0);
	deleteAndZero(currentAudioFileSource);

	AudioFormatManager formatManager;
	formatManager.registerBasicFormats();

	AudioFormatReader* reader = formatManager.createReaderFor(newFile);

	if (reader != 0)
	{
        DBG("FileLoadingProgram::reader!=0");
		currentAudioFileSource = new AudioFormatReaderSource(reader, true);

		audioTransportSource.setSource(currentAudioFileSource, 32768, 
                                       &thread, reader->sampleRate);
        return true;

	}
    else
    {
        DBG("FileLoadingProgram::reader=0");
        AlertWindow::showMessageBox(AlertWindow::WarningIcon,
                                "Que Player",
                                "Couldn't open file!\n\n");
        return false;
    }
}

void AudioQuePlayer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
	audioTransportSource.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void AudioQuePlayer::releaseResources()
{
	audioTransportSource.releaseResources();
}

void AudioQuePlayer::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill)
{
	audioTransportSource.getNextAudioBlock(bufferToFill);
}