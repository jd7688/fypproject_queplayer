/*==============================================================================
    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell
==============================================================================*/

#include "Audio.h"

//Constructor/Destructor===============================================================
Audio::Audio(void)
{
	audioDeviceManager.initialiseWithDefaultDevices(0, 2); //0 inputs, 2 outputs
	
	for(int index = 0; index < noAudioQuePlayers; index++)
	{
		audioSourceMixer.addInputSource(&audioQuePlayer[index], true);
	}
    
	audioDeviceManager.addAudioCallback (this);
    audioSourcePlayer.setSource(&audioSourceMixer);
}

Audio::~Audio(void)
{
	//Delete AudioDeviceManager Functions==========
    audioDeviceManager.removeAudioCallback (this);

	//Delete AudioSourcePlayer Functions===========
	audioSourceMixer.removeAllInputs();
    audioSourceMixer.releaseResources();
}

//Functions to return audio devices=====================================================
AudioDeviceManager& Audio::getAudioDeviceManager(void)
{
	return audioDeviceManager; 
}

MixerAudioSource& Audio::getAudioSourceMixer(void)
{ 
	return audioSourceMixer;
}

//Audio Device Functions================================================================
void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
	// get the audio from our file player - player puts samples in the output buffer
	audioSourcePlayer.audioDeviceIOCallback(inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);

    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
	audioSourcePlayer.audioDeviceAboutToStart(device);
}

void Audio::audioDeviceStopped(void)
{
	audioSourcePlayer.audioDeviceStopped();
}