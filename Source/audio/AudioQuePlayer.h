/*====================================================================
AudioQuePlayer.h
====================================================================*/

#ifndef AUDIOQUEPLAYER_H_INCLUDED
#define AUDIOQUEPLAYER_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

class AudioQuePlayer	: 	public AudioSource
							//public ChangeListener
{
public:
	//=================================================================
	//MainStuff
	AudioQuePlayer();
	~AudioQuePlayer();

	//====================================================================
	//Audio
	void setPlaying(const bool newState);

	bool isPlaying() const;
    
    void setPlayerState(bool newState);
    bool isBusy();  //true == busy, false == avalible.

	bool loadFile(const File& newFile);

	void prepareToPlay(int samplesPerBlockExpected, double sampleRate);
	void releaseResources();
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill);

private:
	AudioFormatReaderSource* currentAudioFileSource;
	AudioTransportSource audioTransportSource;
    bool playerState; //true == busy, false == avalible.

	TimeSliceThread thread;
};

#endif