/*==============================================================================
    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell
==============================================================================*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#define noAudioQuePlayers 8

#include "../../JuceLibraryCode/JuceHeader.h"
#include "AudioQuePlayer.h"
#include "AudioSourceMixer.h"
//#include "../Source/quecontroller/QueController.hpp"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
	//Constructor/Destructor===============================================================
    /** Constructor */
    Audio(void);
    
    /** Destructor */
    ~Audio(void);
    
    //Functions to return audio devices=====================================================
	AudioDeviceManager& getAudioDeviceManager(void);
    MixerAudioSource& getAudioSourceMixer(void);

	//Audio Device Functions================================================================
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped(void) override;

private:
	//AudioDeviceChain==============================================
    AudioDeviceManager audioDeviceManager;
	AudioSourcePlayer audioSourcePlayer;
	MixerAudioSource audioSourceMixer;
	AudioQuePlayer audioQuePlayer[noAudioPlayers];
};
#endif  // AUDIO_H_INCLUDED