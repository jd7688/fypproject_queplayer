/*==============================================================================
//  AudioSourceMixer.cpp
//  JuceBasicAudio
//  Created by Joseph D'Souza on 05/01/2017.
==============================================================================*/

#include "AudioSourceMixer.h"

AudioSourceMixer::AudioSourceMixer() : thread("FilePlayThread")
{
    int index;

	thread.startThread();
    
    for (index = 0; index < noAudioPlayers; index++)
    {
        audioSourceMixer.addInputSource(&audioQuePlayer[index], false);
    }
    
    audioSourceMixer.prepareToPlay(512, 44100);
}

AudioSourceMixer::~AudioSourceMixer()
{
	thread.stopThread(100);
}

void AudioSourceMixer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    int index;
    
    for (index = 0; index < noAudioPlayers; index++)
    {
        audioSourceMixer.prepareToPlay(samplesPerBlockExpected, sampleRate);
    }
}

void AudioSourceMixer::releaseResources()
{
    int index;
    
    for (index = 0; index < noAudioPlayers; index++)
    {
        audioSourceMixer.releaseResources();
    }
}

void AudioSourceMixer::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    int index;
    
    for (index = 0; index < noAudioPlayers; index++)
    {
        audioSourceMixer.getNextAudioBlock(bufferToFill);
    }
}

//AudioSourceMixer==================================================
MixerAudioSource* AudioSourceMixer::getAudioSourceMixer()
{
    return &audioSourceMixer;
}

//AudioQuePlayerFunctions===========================================
AudioQuePlayer* AudioSourceMixer::getAvalibleAudioQuePlayer()
{
    int index = 0;
	bool test = true;
    
    do
    {
        test = audioQuePlayer[index].isBusy();
    }while (test == true && index <noAudioPlayers);
    
    if (test == false)
        return &audioQuePlayer[index];
    else
        return NULL;
}

void AudioSourceMixer::stopAll()
{
	int index = 0;
    for (index = 0; index < noAudioPlayers; index++)
    {
        audioQuePlayer[index].setPlaying(false);
        audioQuePlayer[index].setPlayerState(false);
    }
}
//==============================================================================





