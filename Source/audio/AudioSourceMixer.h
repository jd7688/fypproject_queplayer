/*==============================================================================
//  AudioSourceMixer.h
//  JuceBasicAudio
//  Created by Joseph D'Souza on 05/01/2017.
==============================================================================*/

#ifndef AudioSourceMixer_h
#define AudioSourceMixer_h

#define noAudioPlayers 8

#include "../../JuceLibraryCode/JuceHeader.h"
#include "AudioQuePlayer.h"

class AudioSourceMixer  :   public AudioSource
{
public:
    AudioSourceMixer();
    ~AudioSourceMixer();
    
    //AudioSource======================================================
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate);
    void releaseResources();
    void getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill);
    
    //AudioSourceMixer=================================================
    MixerAudioSource* getAudioSourceMixer();
    
    //AudioQuePlayerFunctions==========================================
    AudioQuePlayer* getAvalibleAudioQuePlayer();
    void stopAll();
    
private:
    MixerAudioSource audioSourceMixer;
    AudioQuePlayer audioQuePlayer[noAudioPlayers];

	TimeSliceThread thread;
};


#endif /* AudioSourceMixer_h */
